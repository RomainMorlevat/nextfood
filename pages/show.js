import Layout from '../components/layout'
import ProductCard from '../components/productCard'
import fetch from 'isomorphic-unfetch'

const Page = ({ productCode, product, searchTerms }) => {
  return (
    <Layout>
      <div className='md:flex'>
        <div className='m-auto'>
          <ProductCard product={product} searchTerms={searchTerms} />
        </div>
      </div>
    </Layout>
  )
}

Page.getInitialProps = async ({ query }) => {
  const baseURL = 'https://world.openfoodfacts.org/api/v0/product/'
  const resp = await fetch(baseURL + query.productCode)
  const json = await resp.json()
  return { product: json.product, searchTerms: query.searchTerms }
}

export default Page
