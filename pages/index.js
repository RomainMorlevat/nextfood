import { useEffect, useState } from 'react'
import fetch from 'isomorphic-unfetch'

import ListItem from '../components/listItem'
import Layout from '../components/layout'
import SearchForm from '../components/searchForm'

const Home = ({ products, _searchTerms }) => {
  const [foods, setFoods] = useState([])
  const [searchTerms, setSearchTerms] = useState('')

  useEffect(() => {
    setFoods(products)
    setSearchTerms(_searchTerms)
  }, [])

  const handleSearch = (foodName) => {
    setSearchTerms(foodName)

    const baseURL = 'https://world.openfoodfacts.org/cgi/search.pl?search_simple=1&action=process&json=1&search_terms='
    fetch(baseURL + foodName)
      .then(r => r.json())
      .then(data => {
        console.log(data)
        setFoods(data.products)
      })
  }

  return (
    <Layout>
      <SearchForm handleSearch={handleSearch} />

      <div className='md:flex'>
        <div className='m-auto'>
          {foods.map((product) => {
            return (
              <ListItem key={product._id} product={product} searchTerms={searchTerms} />
            )
          }
          )}
        </div>
      </div>
    </Layout>
  )
}

Home.getInitialProps = async ({ query }) => {
  if (query.searchTerms === undefined) { return { products: [] } }

  const baseURL = 'https://world.openfoodfacts.org/cgi/search.pl?search_simple=1&action=process&json=1&search_terms='
  const resp = await fetch(baseURL + query.searchTerms)
  const json = await resp.json()
  return { products: json.products, _searchTerms: query.searchTerms }
}

export default Home
