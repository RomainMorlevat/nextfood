import Head from 'next/head'
import '../styles/index.css'

export default ({ children, title = 'nextFood' }) => (
  <>
    <Head>
      <title>{title}</title>
      <meta charSet='utf-8' />
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
    </Head>

    <div>
      {children}
    </div>
  </>
)
