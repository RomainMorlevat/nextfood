const SearchForm = ({ handleSearch }) => {
  const handleSubmit = (event) => {
    event.preventDefault()
    handleSearch(event.target.food.value)
  }

  return (
    <header className='bg-green-800 flex justify-between items-center sm:px-4 py-2 w-full'>
      <form className='m-auto' onSubmit={handleSubmit}>
        <input
          className='appearance-none border focus:outline-none focus:shadow-outline
                 leading-tight px-3 py-2 rounded text-gray-700 mr-2'
          id='food'
          placeholder='Type food name'
          type='text'
        />
        <input
          className='bg-blue-500 focus:outline-none focus:shadow-outline
                 font-bold hover:bg-blue-700 px-4 py-2 rounded text-white'
          type='submit'
          value='Search'
        />
      </form>
    </header>
  )
}

export default SearchForm
