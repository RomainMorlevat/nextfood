import Link from 'next/link'

const ListItem = ({ product, searchTerms }) => {
  return (
    <Link href={{ pathname: '/show', query: { productCode: product._id, searchTerms: searchTerms } }}>
      <div className='flex items-center rounded-lg bg-white shadow-lg overflow-hidden cursor-pointer mt-2'>
        <img className='h-32 w-32 flex-shrink-0' src={product.image_url} alt={`${product.product_name_fr} image`} />
        <div className='px-6 py-4'>
          <h3 className='text-lg font-semibold text-gray-800'>{product.product_name_fr}</h3>
          <p className='text-green-600'>{product.nutriments.energy_value} {product.nutriments.energy_unit}</p>
          <div className='mt-4'>
            <p href='#' className='text-green-600 font-semibold text-sm'>
              Protéines : {product.nutriments.proteins_100g} {product.nutriments.proteins_unit} /
              Lipides : {product.nutriments.fat_100g} {product.nutriments.fat_unit} /
              Glucides : {product.nutriments.carbohydrates_100g} {product.nutriments.carbohydrates_unit}
            </p>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default ListItem
