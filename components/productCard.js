import Router from 'next/router'

import Button from './button'

const ProductCard = ({ product, searchTerms }) => {
  const handleClick = () => {
    Router.push({
      pathname: '/',
      query: { searchTerms: `${searchTerms}` }
    })
  }

  return (
    <>
      <Button onClick={handleClick} text='Back' />
      <div className='m-auto max-w-sm border rounded overflow-hidden shadow bg-white'>
        <img className='w-full object-cover h-64' src={product.image_url} alt={`${product.product_name_fr} image`} />
        <div className='px-6 py-4'>
          <h4 className='font-medium text-xl mb-2'>{product.product_name_fr}</h4>
          <ul>
            <li>Énergie : {product.nutriments.energy_value} {product.nutriments.energy_unit}</li>
            <li>Protéines : {product.nutriments.proteins_100g} {product.nutriments.proteins_unit}</li>
            <li>Lipides : {product.nutriments.fat_100g} {product.nutriments.fat_unit}</li>
            <ul className='font-light pl-4 text-sm'>
              <li>Saturés : {product.nutriments.saturatedFat_100g} {product.nutriments.saturatedFat_unit}</li>
            </ul>
            <li>Glucides : {product.nutriments.carbohydrates_100g} {product.nutriments.carbohydrates_unit}</li>
            <ul className='font-light pl-4 text-sm'>
              <li>Sucre : {product.nutriments.sugars_100g} {product.nutriments.sugars_unit}</li>
            </ul>
          </ul>
        </div>
      </div>
    </>
  )
}

export default ProductCard
